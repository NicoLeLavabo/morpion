/** @type {entier} */
let scorePlayer1 = 0;

/** @type {entier} */
let scorePlayer2 = 0;

/**
 * function for Display an array
 * @author Nicolas
 * @param {(entier | Array)} tab 
 * @return an array 3x3
 */
function displayTab(tab) {
    for (i = 0; i < 3; i++) {
        let row = [];
        for (j = 0; j < 3; j++) {
            row.push(tab[i][j]);
        }
        console.log(row);
    }
}
/**
 * function for restart game or exit
 * @param {string} scorePhrase 
 * @param {(entier | Array)} tab
 * @return new game or exit the game
 */
function nul(scorePhrase, tab) {
    let result = 0;
    let test = false;
    for (i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (tab[i][j] != '_') {
                result++;
            }
        }
    }
    if (result === 9) {
        test = true;
        alert('Match nul !');
        let rejouez = prompt('Voulez-vous rejouer ? oui/non');
        if (rejouez == 'oui') {
            morpion();
        }
        else if (rejouez == 'non') {
            console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            return 'Bonne journée';
        }
        console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
    }
    return test;
}
/**
 * Function for init an array 3x3 with '_' on each case
 * @author Nicolas
 * @param {(entier | Array)} tab
 * @return empty array
 */
function initialize(tab) {
    for (i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            tab[i][j] = '_';
        }
    }
    return displayTab(tab);
}

/**
 * function for display if user win
 * @author Nicolas
 * @param {string} scorePhrase 
 * @param {(entier | Array)} tab
 * @return true and console log if a user win
 */
function win(scorePhrase, tab) {
    for (i = 0; i < 3; i++) {

        if (tab[i][0] === tab[i][1] && tab[i][1] === tab[i][2] && tab[i][0] != '_') {
            if (tab[i][0] === 'X') {
                console.log('Bravo joueur avec les X, vous avez gagné');
                scorePlayer1++;
                console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            }
            else {
                console.log('Bravo joueur avec les O, vous avez gagné');
                scorePlayer2++;
                console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            }
            return true;
        }
    }
    for (j = 0; j < 3; j++) {
        if (tab[0][j] === tab[1][j] && tab[1][j] === tab[2][j] && tab[0][j] != '_') {
            if (tab[0][j] === 'X' || tab[1][j] === 'X' || tab[2][j] === 'X') {
                console.log('Bravo joueur avec les X, vous avez gagné');
                scorePlayer1++;
                console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            }
            else {
                console.log('Bravo joueur avec les O, vous avez gagné');
                scorePlayer2++;
                console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');

            }
            return true;
        }
    }
    if (tab[2][0] === tab[1][1] && tab[1][1] === tab[0][2] && tab[2][0] != '_') {
        if (tab[2][0] === 'X' || tab[1][1] === 'X' || tab[0][2] === 'X') {
            console.log('Bravo joueur avec les X, vous avez gagné');
            scorePlayer1++;
            console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            return true;
        }
        else {
            console.log('Bravo joueur avec les O, vous avez gagné');
            scorePlayer2++;
            console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            return true;
        }


    }
    else if (tab[0][0] === tab[1][1] && tab[1][1] === tab[2][2] && tab[0][0] != '_') {
        if (tab[0][0] === 'X' || tab[1][1] === 'X' || tab[2][2] === 'X') {
            console.log('Bravo joueur avec les X, vous avez gagné');
            scorePlayer1++;
            console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            return true;
        }
        else {
            console.log('Bravo joueur avec les O, vous avez gagné');
            scorePlayer2++;
            console.log(scorePhrase + scorePlayer1 + ' (X) à ' + scorePlayer2 + ' (O)');
            return true;
        }

    }
    else {
        return false;
    }
}

/**
 * principal function of tic-tac-toe game
 * @author Nicolas
 * 
 */
function morpion() {
    let grid = [[], [], []];
    let playerTurn = true;
    let scoreAnswer = 'Le score est de : ';

    initialize(grid);
    while (!win(scoreAnswer, grid)) {
        let coordonneesX = prompt('Quel ligne ?');
        let coordonneesY = prompt('Quel colonne ?');

        for (i = 0; i < grid.length; i++) {
            for (j = 0; j < grid.length; j++) {
                if (playerTurn) {
                    grid[coordonneesX][coordonneesY] = 'X';
                    playerTurn = false;
                }
                else {
                    grid[coordonneesX][coordonneesY] = 'O';
                    playerTurn = true;
                }
            }

        }
        displayTab(grid);
        nul(scoreAnswer, grid);
        console.log('😍😍😍😍😍😍😍😍😍');
    }
    let rejouez = prompt('Voulez-vous rejouer ? oui/non');
    if (rejouez == 'oui') {
        console.log('score p1 ' + scorePlayer1);
        console.log('score p2 ' + scorePlayer2);
        morpion();
    }
    else if (rejouez == 'non') {
        console.log('Bonne journée');
    }
}
